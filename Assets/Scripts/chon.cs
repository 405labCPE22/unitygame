﻿using UnityEngine;
using System.Collections;

public class chon : MonoBehaviour {

	private bool isLeft;
	public int attackPower;
	private int h;
	private int v;
	Vector3 moveDistance; 
	public int speed;
	// Use this for initialization
	void Start () {
		randomWalk();

	}

	// Update is called once per frame
	void Update () {
		if(isLeft){
			//left
			h =  -1;
			// กําหนดทิศทางการเคล่ือนที่ ด้ วยค่ า(h,v) --> (x = h,y = 0,z = v) 
			moveDistance.Set (h,0.0f,0.0f);
			// ระยะทาง = ทิศทาง (เวกเตอร์ ขนาดเท่ ากับ1) x ความเร็ว x เวลา 
			moveDistance = moveDistance.normalized*speed*Time.deltaTime;
		}else{
			//Right
			h = 1;
			// กําหนดทิศทางการเคล่ือนที่ ด้ วยค่ า(h,v) --> (x = h,y = 0,z = v) 
			moveDistance.Set (h,0.0f,0.0f);
			// ระยะทาง = ทิศทาง (เวกเตอร์ ขนาดเท่ ากับ1) x ความเร็ว x เวลา 
			moveDistance = moveDistance.normalized*speed*Time.deltaTime;
		}

		// จากตําแหน่ งเดิมย้ ายไปอีก moveDistance 
		this.GetComponent<Rigidbody2D>().MovePosition(transform.position + moveDistance);
	}

	void randomWalk(){
		int rv = Random.Range(-1, 1);
		if(rv <= 0 ){
			this.isLeft = false;
		}else{
			this.isLeft =  true;
		}

	}

	public void moveInverse(bool flipToX){

		gameObject.GetComponent<SpriteRenderer>().flipX = flipToX;
	}
	public void walk(){

	}
	void OnCollisionEnter2D(Collision2D obj){

		//If Player
		if(obj.gameObject.tag == "Player"){
			//Decrease power

			obj.gameObject.GetComponent<PlayerController>().decreasePower(attackPower);
		}else if(obj.gameObject.tag == "doll"){
			//Left

			this.isLeft = false;
			this.moveInverse(false);
		}else if(obj.gameObject.tag == "WallLeft"){
			//Right

			this.isLeft = true;
			this.moveInverse(true);
		}

	}
}
