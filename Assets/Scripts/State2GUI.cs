﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class State2GUI : MonoBehaviour {

	// Use this for initialization
	int hp;
	int life;


	// Use this for initialization
	void Start () {
		GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().hp = PlayerPrefs.GetInt("PlayerScore");
		GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().life = PlayerPrefs.GetInt("PlayerScore1");
	}

	// Update is called once per frame
	void Update () {
		

	}


	void OnGUI() {
		GUIStyle style = new GUIStyle();
		style.fontSize = 30;
		style.normal.textColor = Color.red;
		hp = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().hp;
		GUI.Label(new Rect(10,10,100,20), "Hp : "+hp, style );	

		GUIStyle style2 = new GUIStyle();
		style2.fontSize = 20;
		life = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>().life;
		GUI.Label(new Rect(120,10,100,20), "Score : "+life, style );


	}
}
