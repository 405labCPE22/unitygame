﻿using UnityEngine;
using System.Collections;

public class BubbleController : MonoBehaviour {

	public float speedBubble;
	bool checkWaiting;
	//Y -5 to 5

	// Use this for initialization
	void Start () {

		speedBubble = 0.1f;
		StartCoroutine("startMove");
	}
	
	// Update is called once per frame
	void Update () {
		if(!checkWaiting){
			transform.position = transform.up*speedBubble + transform.position;
		}

		if(transform.position.y>6 && !checkWaiting){
			checkWaiting = true;
			StartCoroutine("startMove");
		}
	}

	IEnumerator startMove(){
		int ranStart = Random.Range(1,6);
		yield return new WaitForSeconds(ranStart);
		transform.position = new Vector2(transform.position.x,-5);
		checkWaiting = false;

	}
}
