﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

	public AudioClip getCoin;

    AudioSource myAudio;

	void Start () {

		myAudio = GetComponent<AudioSource>();


	}
	

	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.tag == "Player") // ถ้า GameObject ที่มาชนคือ Player เล่นคลิปเสียงการชน 
		{
			//myAudio.clip = getCoin;

			//myAudio.Play();
			Destroy(gameObject); // ตรงนี้ต้องปรับเองว่า จะทำลายเร็วหรือช้า ต้องดูความเหมาะสม
		}
	}

}
