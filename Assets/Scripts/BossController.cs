﻿using UnityEngine;
using System.Collections;

public class BossController : MonoBehaviour {

	public GameObject attackItem;
	public int HpBoss = 1000;

	public int powerAttack;
	// Use this for initialization
	void Start () {
		StartCoroutine("SpawnAttackItem",3.0f);

	}
	
	// Update is called once per frame
	void Update () {
		if(this.HpBoss <= 0){
			//It's die
		
			Destroy(gameObject);
		}
	}
	IEnumerator SpawnAttackItem(float delayTime){
		while(true){ 
			Instantiate(attackItem,transform.position,Quaternion.identity);
			yield return new WaitForSeconds(delayTime);
		} 
	}
	void OnTriggerEnter2D(Collider2D obj){
		if(obj.gameObject.CompareTag("EggBullet")){
			int attackBoss = GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerController> ().attackBoss;
			HpBoss -= attackBoss;
		}
	}
}
